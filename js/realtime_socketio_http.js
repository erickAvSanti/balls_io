var appgame = appgame || {};

appgame.Realtime = (
	function(){

		var _instance = null;  
		var _socket = null;
		var _data = null;
		var _mousePos = new THREE.Vector2();
		var _mousePosLast = new THREE.Vector2();
		var _mouse3DPos = new THREE.Vector3();
		var _enemies = null;
		var _userid = null;

		var _calcLookAt = function(){
			if(!w_wgl.camera)return;
			_mouse3DPos.set(
			    (_mousePos.x / window.innerWidth) * 2 - 1,
			    - (_mousePos.y / window.innerHeight) * 2 + 1,
			    0.5
			);
			_mouse3DPos.unproject(w_wgl.camera); 
			_mouse3DPos.sub(w_wgl.camera.position); 
			_mouse3DPos.normalize(); 

			t = (-1)*w_wgl.camera.position.z/_mouse3DPos.z; 
			x = w_wgl.camera.position.x + t*_mouse3DPos.x;
			z = w_wgl.camera.position.z + t*_mouse3DPos.z;
			y = w_wgl.camera.position.y + t*_mouse3DPos.y; 
			_mouse3DPos.set(x,y,z); 
			if(_mousePosLast.x!=_mousePos.x && _mousePosLast.y!=_mousePos.y){
				_socket.emit("lookAt",[_mouse3DPos.x,_mouse3DPos.y,_mouse3DPos.z]);
			} 
		};

		Realtime = function(){

		};
		Realtime.prototype = {
			constructor:Realtime, 
			test:function(){
				console.log("hello from Realtime object");
			},
			start:function(){ 
				(_enemies = appgame.Enemies.instance(this)).start();  
				//_socket = io.connect('http://192.168.1.40:50001'); 
				_socket = io.connect('http://192.168.1.12:50001'); 
				_socket.on('onconnected', function( data ) {  
					_userid = data.userid; 
					_data = data;
					console.log(data);
			  		console.log( 'Connected successfully to the socket.io server. My server side ID is ' + data.userid );
			  	}); 
			  	$(window).keypress(function(_evt){
			  		console.log(_evt.which);
			  		if(_evt.which==100){
			  			_socket.emit("d",1);
			  		}
			  	});
			  	$(window).mousemove(function(_evt){
			  		_mousePos.set(_evt.clientX,_evt.clientY);
			  	});
			  	_socket.on("clients",function(data){
			  		_instance.parseClients(data);
			  	});

			  	setInterval(_calcLookAt,1000/60);
			}, 
			parseClients:function(data){
				if(_enemies)_enemies.parseClients(data,_userid);
			},
			setData:function(data){
				_data = data;
			},
			render:function(_mesh){
				if(_data && _mesh){
					console.log();
					_mesh.userid = _userid; 
					_mesh.position.fromArray(_data.position);
					_mesh.visible = true;  
					w_wgl.camera.position.x =_mesh.position.x;
					w_wgl.camera.position.y =_mesh.position.y;
					w_wgl.camera.lookAt(_mesh.position); 
					_mesh.lookAt(_mouse3DPos); 
				}
				if(_enemies)_enemies.render();  
			},
			animate:function(){ 
			}  
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new Realtime();
					return _instance;
				}
				return null;
			}
		};
	}
)();
