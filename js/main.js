$(window).ready(
	function(evt){  
		setCanvasDim();
		$(window).resize(
			function(evt){
				setCanvasDim();
			}
		);
		(window.appMain = appgame.Main).start();
	}
); 
function setCanvasDim(){
	window.w_width = window.innerWidth;
	window.w_height = window.innerHeight;
	$("canvas").each(
		function(index,obj){
			var _this = $(obj);
			var _id = _this.attr("id");
			if(
				_id!="webgl" &&  
				_id!="canvas-assets" &&  
				_id!="canvas-touch" 
			){
				return;
			}
			_this.css("width",w_width+"px").css("height",w_height+"px");
			_this.attr("width",w_width).attr("height",w_height);
		}
	);
}

var appgame = appgame || {};
appgame.Main = (
	function(){

		var _instance = null;

		var _router = null;
		var _zona_a = null;
		var _ambientLight = new THREE.AmbientLight( 0xffffff); 
		var _directionalLight = new THREE.DirectionalLight( 0xaaaaaa, 0.1 ); 

		 

		var _cameraZPosInit = 10;
		var _cameraYPosInit = 0;
		var _cameraYPosInit_min = _cameraYPosInit;
		var _cameraXPosInit = 0;

		var _cameraAngle_def = 60;
		var _cameraAngle = _cameraAngle_def;

		var _fullScreen = null; 
		var _realTime = null; 
		var _assets = null; 
		var _enemies = null; 
		var _stats = null;

		_debug_touch = true;

		window._lib_game_finish = false;

		var _mesh = null;
		var _mesh_added = false;

		Main = function(){ 

			window.w_wgl 		= document.getElementById("webgl");
 			window.w_cassets 	= document.getElementById("canvas-assets");
 			window.w_ctouch 	= document.getElementById("canvas-touch");
 			if(window.w_cassets && window.w_cassets.getContext)window.w_cassets_ctx = window.w_cassets.getContext("2d");
 			if(window.w_ctouch && window.w_ctouch.getContext)window.w_ctouch_ctx = window.w_ctouch.getContext("2d");

		};
		Main.prototype = {
			constructor:Main,
			start:function(){
				window._lib_lvl = 1;
				var scene = new THREE.Scene();
				scene.background = new THREE.Color(0.5,0.5,0.9);
				var camera = new THREE.PerspectiveCamera( _cameraAngle, w_width / w_height, 0.1, 1000 );

				var renderer = new THREE.WebGLRenderer({canvas:w_wgl}); 
				renderer.setSize(w_width,w_height);
				renderer.setPixelRatio(window.devicePixelRatio);
    	
  				_directionalLight.position.set(0,0,10); 
  				_directionalLight.target = new THREE.Object3D();
  				_directionalLight.target.position.set(0,0,0);


				scene.add( _ambientLight );
				scene.add( _directionalLight ); 
				scene.add( _directionalLight.target ); 
				camera.position.set(_cameraXPosInit,_cameraYPosInit,_cameraZPosInit);
				camera.lookAt(new THREE.Vector3());

				w_wgl.scene = scene;
				w_wgl.camera = camera;
				w_wgl.renderer = renderer; 
				w_wgl.playing = false;
				w_wgl.rqsAnim = true;

				w_wgl._lib_currTime = Date.now();
				w_wgl._lib_lastTime = w_wgl._lib_currTime;
 

				var _this = this;
				$(window).resize(
					function(evt){
						_this.resize();
					}
				);
				$(window).focus(
					function(evt){
						_this.focus();
					}
				);
				$(window).blur(
					function(evt){ 
						_this.blur();
					}
				);

				w_ctouch.addEventListener("touchstart",_this.touchstart,false);
				w_ctouch.addEventListener("touchend",_this.touchend,false);
				w_ctouch.addEventListener("touchcancel",_this.touchcancel,false);
				w_ctouch.addEventListener("touchleave",_this.touchleave,false);
				w_ctouch.addEventListener("touchmove",_this.touchmove,false);

				_stats = new Stats();
				_stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
				document.body.appendChild( _stats.dom );

				_this.animate();
 
				(_fullScreen = appgame.FullScreen.instance()).start();  
				(_realTime = appgame.Realtime.instance()).start();  
				(_assets = appgame.Assets.instance()).start();  

				_this.mostrarPaneldeBienvenida();
				_this.procesarNivelJuego();

				_this.ctrlButton();
				_this.parseVendor();
				$("#play").click(function(_evt){
					$("#modal_bienvenida").modal("hide");
						/*window._lib_game_paused = false;
						window._lib_game_finish = false;
						window._lib_game_reset = true;*/
				});

			},
			ctrlButton:function(){
				$("body").append("<div id='settings'></div>"); 
				$("#settings").click(
					function(_evt){ 
						$("#modal_bienvenida").modal("show");
						//window._lib_game_paused = true;
					}
				);
			},
			touchstart:function(_evt){  
			},
			touchend:function(_evt){  
			},
			touchcancel:function(_evt){  
			},
			touchleave:function(_evt){  
			},
			touchmove:function(_evt){  
			}, 
			animate:function(){
				if(w_wgl.rqsAnim){
					w_wgl.rqs_anim_id = requestAnimationFrame( _instance.animate );
				}  
				_stats.begin();
				if(!w_wgl._lib_currTime){
					w_wgl._lib_currTime = Date.now();
					w_wgl._lib_lastTime = w_wgl._lib_currTime; 
				}
				w_wgl._lib_lastTime = w_wgl._lib_currTime;
				w_wgl._lib_currTime = Date.now();

				if(w_ctouch_ctx)w_ctouch_ctx.clearRect(0,0,w_width,w_height);
				if(_fullScreen)_fullScreen.render(); 
				if(_assets)_assets.render();  
				if(_assets && !_mesh){
					_mesh = _assets.getNave();
					if(_mesh)_mesh.name_mesh = _assets.getPlayerNameMesh("Sin nombre");
				}
				if(_mesh && !_mesh_added){
					_mesh_added = true;
					w_wgl.scene.add(_mesh);
					w_wgl.scene.add(_mesh.name_mesh);
				}
				if(_realTime)_realTime.render(_mesh); 
				w_wgl.renderer.render(w_wgl.scene, w_wgl.camera);
				_stats.end();
			},
			blur:function(){
				//this.stopAnimation();
			},
			focus:function(){
				this.playAnimation();
			}, 
			stopAnimation:function(){
				w_wgl.rqsAnim = false;
				if(w_wgl.rqs_anim_id){
					cancelAnimationFrame(w_wgl.rqs_anim_id);
				}
			},
			playAnimation:function(){
				if(w_wgl.rqs_anim_id)cancelAnimationFrame(w_wgl.rqs_anim_id);
				w_wgl.rqsAnim = true;
				this.animate();
			},
			resize:function(){
				w_wgl.camera.aspect = w_width / w_height;
    			w_wgl.camera.updateProjectionMatrix();
				w_wgl.renderer.setSize(w_width,w_height);
				if(_fullScreen)_fullScreen.resize();  
			},
			parseVendor:function(){
				if(!window._lib_vendor && w_wgl.renderer){
					var ctx = w_wgl.renderer.getContext();
					var info = w_wgl.renderer.extensions.get("WEBGL_debug_renderer_info");
					window._lib_vendor = {};
					window._lib_vendor.vendor 		= ctx.getParameter(ctx.VENDOR);
					window._lib_vendor.renderer 	= ctx.getParameter(ctx.RENDERER);
					window._lib_vendor.u_vendor 	= ctx.getParameter(info.UNMASKED_VENDOR_WEBGL);
					window._lib_vendor.u_renderer 	= ctx.getParameter(info.UNMASKED_RENDERER_WEBGL);

					$("#cpu_info1").html(_lib_vendor.vendor);
					$("#cpu_info2").html(_lib_vendor.renderer);
					$("#cpu_info3").html(_lib_vendor.u_vendor);
					$("#cpu_info4").html(_lib_vendor.u_renderer); 
					$("#cpu_info5").html("#Procesadores: "+navigator.hardwareConcurrency); 

				}
			},
			mostrarPaneldeBienvenida:function(){
				$(document).keyup(function(evt){
					if(evt.which==27){
						var _modal = $("#modal_bienvenida"); 
						if(!_modal.is(":visible")){
							_modal.modal("show");
						}else{
							_modal.modal("hide");
						}
						
					}
				});
				$("#modal_bienvenida").modal("show");
				window._lib_game_paused = true;
				if(false && window.location && !/localhost/.test(window.location.hostname)){
					var _str = ''+
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>'+
						'<ins class="adsbygoogle"'+
						     'style="display:block;"'+
						     'data-ad-format="fluid"'+
						     'data-ad-layout-key="-8m+w-aj+dk+bi"'+
						     'data-ad-client="ca-pub-6209186190244737"'+
						     'data-ad-slot="7742196875"></ins>'+
						'<script>'+
						     '(adsbygoogle = window.adsbygoogle || []).push({});'+
						'</script>';

					var _str2 = ''+
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>'+
						'<ins class="adsbygoogle"'+
					     'style="display:block;"'+
					     'data-ad-format="fluid"'+
					     'data-ad-layout-key="-8n+x-ah+dn+as"'+
					     'data-ad-client="ca-pub-6209186190244737"'+
					     'data-ad-slot="8801742078"></ins>'+
						'<script>'+
						     '(adsbygoogle = window.adsbygoogle || []).push({});'+
						'</script>'; 

					$("#ct-1").html(_str);
					$("#ct-2").html(_str2); 
				}
			},
			procesarNivelJuego:function(){ 
				this.cargarZonaA();
			},
			cancelarCargaActual:function(){
				this.eliminarObjetosDeEscena();
			},
			eliminarObjetosDeEscena:function(){
				if(w_wgl.scene){ 
				} 
			},
			cargarZonaA:function(){
				if(_zona_a)_zona_a.cargarElementos();
			}
		};

		return {
			start:function(){
				if(!_instance){
					_instance = new Main();
					_instance.start();
				}
			}
		};
	}
)();
