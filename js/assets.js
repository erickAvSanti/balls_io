var appgame = appgame || {};

appgame.Assets = (
	function(){

		var _instance = null;
		var _piso = null;  
		var _nave = null;
		var _assets = {} 
		var _loader = new THREE.JSONLoader();

		function createTextCanvas(text, color, font, size) {
			size = size || 24;
			var canvas = document.createElement('canvas');
			var ctx = canvas.getContext('2d');
			var fontStr = (font || 'Arial') + ' ' + (size +'px');
			ctx.font = fontStr; 
			var w = ctx.measureText(text).width;
			var h = Math.ceil(size*1.25);
			canvas.width = w;
			canvas.height = h;
			ctx.font = fontStr;
			ctx.fillStyle = color || 'black';
			ctx.fillText(text, 0, size);
			return canvas;
		}

		function getTextSprite(text,size){
			return new THREE.TextSprite({
			  	textSize: size, 
			  	redrawInterval: 250, 
			  	texture: {
			    	text: text,
			    	//fontFamily: 'Arial, Helvetica, sans-serif',
			    	fontFamily: 'Tahoma, Geneva, sans-serif',
			    	fontWeight: 'bold', 
			    	lineWidth: 0.1, 
			    	strokeStyle: '#666', 
			  	},
			  	material: {
			    	color: 0xdedede, 
			    	fog: true, 
			  	},
			});
		}



		Assets = function(){

		};
		Assets.prototype = {
			constructor:Assets, 
			test:function(){
				console.log("hello from Assets object");
			},
			start:function(){  
				this.loadFile("piso","./modelos/piso.json",true);
				this.loadFile("nave","./modelos/nave.json");
				this.loadFile("bullet","./modelos/bullet.json");
			}, 
			render:function(){ 
				if(!_piso && "piso" in _assets){
					_piso = _assets["piso"];
					_piso.rotateX(Math.PI/2);
					_piso.position.z = -0.1;
				}
				if(!_nave && "nave" in _assets){
					_nave = _assets["nave"];
					_nave.up.y=0; 
					_nave.up.z=1; 
				}
			},
			getNave:function(){
				return _nave;
			},
			get:function(_str){
				return _str in _assets ? _assets[_str] : null;
			},
			animate:function(){ 
			}, 
			loadFile:function(idx,obj,_append){
				_loader.load( 
					obj,  
					function ( geometry, materials ) { 
						var object;  
						/*for(var _index in materials){
							var _mat = materials[_index]; 
							_mat.side = THREE.FrontSide;
						} */
						object = new THREE.Mesh( geometry, materials ); 
						if(_append)w_wgl.scene.add( object ); 
						_assets[idx] = object;
					},
					function(){

					},
					function(xhr){
						console.log(xhr);
					}
				);
			},
			getPlayerNameMesh:function(text){ 
			    /*
			    var texture = new THREE.Texture(createTextCanvas(text,'black','italic',20)); 
			    texture.needsUpdate = true;
			    var material = new THREE.MeshBasicMaterial({ map: texture,transparent:true});
			    var mesh = new THREE.Mesh( new THREE.PlaneGeometry(2,2,4,4), material );
			    mesh.rotateX(-2*Math.PI);
			    */
			    var mesh = getTextSprite(text,0.5);
			    mesh.text = text;
			    return mesh;
			},
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new Assets(); 
				}
				return _instance;
			}
		};
	}
)();
