var appgame = appgame || {};

appgame.Realtime = (
	function(){

		var _instance = null;  
		var _socket = null;
		var _data = null;
		var _mousePos = new THREE.Vector2();
		var _mousePosLast = new THREE.Vector2();
		var _mouse3DPos = new THREE.Vector3();
		var _enemies = null;
		var _userid = null;
		var _assets = null;

		var _dataToSendServer = function(){
			if(!w_wgl.camera)return;
			_mouse3DPos.set(
			    (_mousePos.x / window.innerWidth) * 2 - 1,
			    - (_mousePos.y / window.innerHeight) * 2 + 1,
			    0.5
			);
			_mouse3DPos.unproject(w_wgl.camera); 
			_mouse3DPos.sub(w_wgl.camera.position); 
			_mouse3DPos.normalize(); 

			t = (-1)*w_wgl.camera.position.z/_mouse3DPos.z; 
			x = w_wgl.camera.position.x + t*_mouse3DPos.x;
			z = w_wgl.camera.position.z + t*_mouse3DPos.z;
			y = w_wgl.camera.position.y + t*_mouse3DPos.y; 
			_mouse3DPos.set(x,y,z); 
			if(_mousePosLast.x!=_mousePos.x && _mousePosLast.y!=_mousePos.y){ 
				var _msg = {
					onlookat:true,
					msg:[_mouse3DPos.x,_mouse3DPos.y,_mouse3DPos.z]
				}; 
				socketSend(_msg,true);
				
			} 
		};
		function socketSend(data,_json = false){
			if(_socket && _socket.readyState == _socket.OPEN){
				_socket.send(_json ? JSON.stringify(data) : data);
			}
		}


		Realtime = function(){

		};
		Realtime.prototype = {
			constructor:Realtime, 
			test:function(){
				console.log("hello from Realtime object");
			},
			onConnected:function(data){ 
				_userid = data.userid; 
				_data = data;
				console.log(data);
		  		console.log( 'Connected successfully to the socket.io server. My server side ID is ' + data.userid );
			},
			start:function(onlySocket = false){ 
				if(!onlySocket){
					(_enemies = appgame.Enemies.instance(this)).start();  
					_assets = appgame.Assets.instance();
				}
				
				//_socket = io.connect('http://192.168.1.40:50001'); 
				_socket = new WebSocket('ws://localhost:50001');
				_socket.onopen = function(_evt){ 
					console.log("open",_evt);
				};
				_socket.onmessage = function(_evt){
					//console.log("message",_evt);
					try{
						var _res =JSON.parse(_evt.data);
						if(_res.onconnected){
							_instance.onConnected(_res.msg);
						}
						if(_res.onclients){
							_instance.parseClients(_res.msg);
						}
					}catch(exc){

					}
				};
				_socket.onclose = function(_evt){
					console.log("close",_evt);
					setTimeout(function(){_instance.start(true)},5000);
				};
				//_socket = io.connect('http://192.168.1.12:50001'); 
				/*_socket.on('onconnected', function( data ) {  
					_userid = data.userid; 
					_data = data;
					console.log(data);
			  		console.log( 'Connected successfully to the socket.io server. My server side ID is ' + data.userid );
			  	}); */
			  	if(!onlySocket){
				  	$(window).keypress(function(_evt){
				  		console.log(_evt.which);
				  		if(_evt.which==100){ 
				  			socketSend({d:true,msg:1},true);
				  		}
				  	});
				  	$(window).mousemove(function(_evt){
				  		_mousePos.set(_evt.clientX,_evt.clientY);
				  	});
				  	/*_socket.on("clients",function(data){
				  		_instance.parseClients(data);
				  	});*/

					$("input[name=player_name]").keyup(function(evt){
						var _obj = $(this);
						if(evt.which==13){
							socketSend({name:_obj.val()},true);
						}
					});
				  	setInterval(_dataToSendServer,1000/60);
			  	}
			}, 
			parseClients:function(data){
				if(_enemies)_enemies.parseClients(data,_userid);
			},
			setData:function(data){
				_data = data;
			},
			render:function(_mesh){
				if(_data && _mesh){
					console.log();
					_mesh.userid = _userid; 
					_mesh.position.fromArray(_data.position);
					_mesh.visible = true;  
					w_wgl.camera.position.x =_mesh.position.x;
					w_wgl.camera.position.y =_mesh.position.y; 
					w_wgl.camera.lookAt(_mesh.position); 
					_mesh.lookAt(_mouse3DPos); 
					if(_mesh.name_mesh){
						if(_assets && _data.name!=_mesh.name_mesh.text && typeof _data.name =="string"){
							w_wgl.scene.remove(_mesh.name_mesh);
							_mesh.name_mesh = _assets.getPlayerNameMesh(_data.name);
							w_wgl.scene.add(_mesh.name_mesh);
						}
						_mesh.name_mesh.position.copy(_mesh.position);
					}
				}
				if(_enemies)_enemies.render();  
			},
			animate:function(){ 
			}  
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new Realtime();
					return _instance;
				}
				return null;
			}
		};
	}
)();
