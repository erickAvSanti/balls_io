<div class="modals">
	<div id="modal_bienvenida" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<?php 
	  		if ( $detect->isMobile() ) {
 				$_modal = "sm";
			}else{
 				$_modal = "sm";
 			}
	  	?>
	  	<div class="modal-dialog modal-<?php echo $_modal;?>"> 
	    	<!-- Modal content-->
	    	<div class="modal-content">
	      		<div class="modal-header"> 
	        		<h4 class="modal-title cFont cColor1">Balls IO</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	      			<ul class="nav nav-tabs">
				  		<li class="active cFont"><a data-toggle="tab" href="#home">Inicio</a></li>
				  		<li class="cFont"><a data-toggle="tab" href="#menu1">Uso/Config</a></li>
				  		<li class="cFont"><a data-toggle="tab" href="#menu2">Extras</a></li>
					</ul>

					<div class="tab-content">
				  		<div id="home" class="tab-pane fade in active">
				  			<div class="form-group">
				  				<br />
				  				<label class="cFont">Nickname</label>
				  				<input class="form-control" type="text" name="player_name" placeholder="your name">
				  			</div>  
				  		</div>
				  		<div id="menu1" class="tab-pane fade"> 
				  		</div>
				  		<div id="menu2" class="tab-pane fade">
				  			<br />
				    		<ul> 
				    			<li>
				    				Información WebGL/GPU/CPU:
				    				<ul>
				    					<li id="cpu_info1"></li>
				    					<li id="cpu_info2"></li>
				    					<li id="cpu_info3"></li>
				    					<li id="cpu_info4"></li>
				    					<li id="cpu_info5"></li> 
				    				</ul>
				    			</li>
				    			<li>Desarrollado por: <a href="http://arquigames.pe" target="_blank">arquigames</a> Versión 1.3.3</li>
				    		</ul>
				  		</div>
					</div>	
	      		</div>
	      		<div class="modal-footer"> 
	      		</div>
	    	</div> 
	  	</div>
	</div>
	<div id="modal_lost" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-md"> 
	    	<!-- Modal content-->
	    	<div class="modal-content">
	      		<div class="modal-header"> 
	        		<h4 class="modal-title cFont cColor1">Balls IO</h4>
	      		</div>
	      		<div class="modal-body">  
	      		</div>
	      		<div class="modal-footer"> 
	      		</div>
	    	</div> 
	  	</div>
	</div>
</div>  