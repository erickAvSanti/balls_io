  
    var cluster = require('cluster');
    var numCPUs = require('os').cpus().length;
    console.log("#cpus: "+numCPUs);

    if(cluster.isMaster){
        console.log("cluster is master");
        for(var _idx=0;_idx<numCPUs;_idx++){
            cluster.fork();
        }
        cluster.on('exit', (worker, code, signal) => {
            console.log("worker:",worker);
            console.log("code:",code);
            console.log("signal:",signal);
            cluster.fork();
        });
    }else{
        var gameport = 50001;  
        var UUID    = require('node-uuid');
        var THREE = require("three-js")(); 

        var ws = require("nodejs-websocket");

        var _max_catch_exception = 50;
        var _count_catch_exception = 0;
      
        var server = ws.createServer(function (conn) {
            conn.player_name = "Sin nombre";
            _onConnection(conn);
            console.log("#sockets : "+_sockets.length);
            conn.on("text", function (str) { 
                try{
                    var _json = JSON.parse(str);
                    if(_json.d){
                        conn.prep_newBullet = true;
                    }
                    if(_json.name){
                        conn.player_name = _json.name;
                        console.log("new player name :"+conn.player_name+", length: "+conn.player_name.length);
                    }
                    if(_json.onlookat){
                        conn.lookAt.x = _json.msg[0];
                        conn.lookAt.y = _json.msg[1];
                        conn.lookAt.z = 0;  
                    }
                }catch(e){
                    if(_count_catch_exception<_max_catch_exception){
                        console.log(e);
                        _count_catch_exception++;
                    } 
                }
            })
            conn.on("binary", function (inStream) { 
                var data = new Buffer(0); 
                inStream.on("readable", function () {
                    var newData = inStream.read();
                    if (newData){
                        data = Buffer.concat([data, newData], data.length+newData.length);
                    }
                });
                inStream.on("end", function () {
                    console.log("Received " + data.length + " bytes of binary data"); 
                })
            });
            conn.on("close", function (code, reason) {
                console.log("Connection closed",code,reason);
                console.log('\t socket disconnected ' + conn.userid ); 
                for(var _idx=0;_idx<_sockets.length;_idx++){
                    var _socket = _sockets[_idx];
                    if(_socket.userid == conn.userid){
                        _sockets.splice(_idx,1);
                    }
                }
                console.log("#sockets after any closed: "+_sockets.length);
            });
            conn.on("error", function (err) {
                //console.log(err); 
            });
        }).listen(gameport);

        var _sockets = [];   
        function _onConnection(socket){
            _sockets.push(socket);
            console.log("#sockets : "+_sockets.length);
            setProperties(socket);
            socket.userid = UUID();
            var _msg = { 
                    onconnected:true,
                    msg:{
                        userid: socket.userid,
                        position:[
                            socket.position.x,
                            socket.position.y,
                            0
                        ] ,
                        lookAt:[
                            socket.lookAt.x,
                            socket.lookAt.y,
                            0
                        ],
                        bullets:[] 
                    }
                };
            var _json = JSON.stringify(_msg); 
            socket.send(
                _json
            ); 
            console.log('\t socket.io:: player ' + socket.userid + ' connected');  
        }
        function createNewBullet(socket){
            var _bullet = null;
            var _find = false;
            for(var _idx= 0; _idx<socket.bullets.length;_idx++){
                _bullet = socket.bullets[_idx];
                if(!_bullet.visible){
                    _find = true;
                    break;
                }
            }
            if(!_find){
                _bullet = {};
                socket.bullets.push(_bullet);
            }
            _bullet.visible = true;
            _bullet.position = socket.position.clone();
            _bullet.direction = socket.vUnit.clone();
            _bullet.velocity = 0.35;
            _bullet.lookAt = new THREE.Vector3();
            console.log("bullet created, #total : "+socket.bullets.length);
        }
        function setInitialPosition(socket){
            var _posX =THREE.Math.randFloat(-10,10);
            var _posY =THREE.Math.randFloat(-10,10);
            socket.position = new THREE.Vector3(_posX,_posY,0);
            socket.lookAt = new THREE.Vector3(_posX,_posY-1,0);
            socket.vUnit = new THREE.Vector3(0,-1,0);
            socket.velocity = 0;
        };
        function setProperties(socket){
            setInitialPosition(socket); 
            socket.bullets = [];
            socket.interval_newBullet_def = 10;
            socket.interval_newBullet = socket.interval_newBullet_def;
        };
        function checkBulletOutside(bullet){
            if(bullet.position.x>100 || bullet.position.x<-100){
                bullet.visible = false;
            }
            if(bullet.position.y>100 || bullet.position.y<-100){
                bullet.visible = false;
            }
        };
        function checkSocketOutside(socket){
            if(socket.position.x>70){
                socket.position.x=70;
            }
            if(socket.position.x<-70){
                socket.position.x=-70;
            }
            if(socket.position.y>70){
                socket.position.y=70;
            }
            if(socket.position.y<-70){
                socket.position.y=-70;
            } 
        };

        var _fps = function(){
            var _packs = [];
            for(var _idx=0;_idx<_sockets.length;_idx++){
                var _socket = _sockets[_idx];
                if(_socket.readyState == _socket.OPEN){
                    if(_socket.interval_newBullet>0){
                        _socket.interval_newBullet--;
                    }else{
                        if(_socket.prep_newBullet){
                            createNewBullet(_socket);
                            _socket.prep_newBullet = false;
                        }
                        _socket.interval_newBullet = _socket.interval_newBullet_def;
                    }
                    var _pack = {};
                    _pack.name = _socket.player_name;
                    _pack.userid = _socket.userid;
                    _pack.lookAt = _socket.lookAt.toArray(); 
                    _socket.vUnit = _socket.lookAt.clone().sub(_socket.position).normalize();
                    var _distance = _socket.lookAt.distanceTo(_socket.position);
                    _distance = THREE.Math.clamp(_distance,0,10);
                    if(_distance>0.1){
                        _socket.velocity = 0.1*(_distance/10);
                    }else{
                        _socket.velocity = 0;
                    }
                    _socket.position.addScaledVector(_socket.vUnit,_socket.velocity);
                    checkSocketOutside(_socket);

                    _pack.position = _socket.position.toArray();

                    var _arr_bullets = [];

                    for(var _idy=0;_idy<_socket.bullets.length;_idy++){
                        var _bullet = _socket.bullets[_idy];
                        if(_bullet.visible){
                            _bullet.lookAt.copy(_bullet.position).addScaledVector(_bullet.direction,1); 
                            _bullet.position.addScaledVector(_bullet.direction,_bullet.velocity);
                            checkBulletOutside(_bullet);
                        }
                        if(_bullet.visible){ 
                            _arr_bullets.push({
                                position:_bullet.position.toArray(),
                                direction:_bullet.direction.toArray(),
                                lookAt:_bullet.lookAt.toArray() 
                            });
                        }
                    }
                    _pack.bullets = _arr_bullets;

                    _packs.push(_pack);
                }else{
                    _sockets.splice(_idx,1);
                }
            }
            if(_packs.length>0){
                _packs = {onclients:true,msg:_packs};
                for(var _idx=0;_idx<_sockets.length;_idx++){
                    _sockets[_idx].sendText(JSON.stringify(_packs));
                }
                
                delete _packs;
            } 
        };
        var _interval = setInterval(_fps,1000/60); 
        console.log("Worker "+process.pid+" started");
    } 
    