  
    var gameport = 50001; 
    var UUID    = require('node-uuid');
    var THREE = require("three-js")();
    var exp = require('express')();
    var app = require('http').createServer(exp);
	var io = require('socket.io')(app); 
    app.listen( gameport );   

    var _sockets = [];

    io.on('connection', function (socket) { 
    	_onConnection(socket);
    });   
    function _onConnection(socket){
    	_sockets.push(socket);
        console.log("#sockets : "+_sockets.length);
    	setProperties(socket);
        socket.userid = UUID(); 
        socket.emit('onconnected', 
        	{ 
        		userid: socket.userid,
        		position:[
        			socket.position.x,
        			socket.position.y,
        			0
        		] ,
        		lookAt:[
        			socket.lookAt.x,
        			socket.lookAt.y,
        			0
        		],
        		bullets:[] 
        	} 
        ); 
        console.log('\t socket.io:: player ' + socket.userid + ' connected'); 
        socket.on('disconnect', function () { 
            console.log('\t socket.io:: socket disconnected ' + socket.userid ); 
        	for(var _idx=0;_idx<_sockets.length;_idx++){
        		var _socket = _sockets[_idx];
        		if(_socket.userid == socket.userid){
        			_sockets.splice(_idx,1);
        		}
        	}
        	console.log("#sockets : "+_sockets.length);
        });  
        socket.on("lookAt",function(data){
        	socket.lookAt.x = data[0];
        	socket.lookAt.y = data[1];
        	socket.lookAt.z = 0;  
        });
        socket.on("d",function(data){
        	if(data==1){
        		socket.prep_newBullet = true;
        	}
        });
    }
    function createNewBullet(socket){
    	var _bullet = null;
    	var _find = false;
    	for(var _idx= 0; _idx<socket.bullets.length;_idx++){
    		_bullet = socket.bullets[_idx];
    		if(!_bullet.visible){
    			_find = true;
    			break;
    		}
    	}
    	if(!_find){
    		_bullet = {};
    		socket.bullets.push(_bullet);
    	}
    	_bullet.visible = true;
		_bullet.position = socket.position.clone();
		_bullet.direction = socket.vUnit.clone();
		_bullet.velocity = 0.2;
		_bullet.lookAt = new THREE.Vector3();
		console.log("bullet created, #total : "+socket.bullets.length);
    }
    function setInitialPosition(socket){
    	var _posX =THREE.Math.randFloat(-10,10);
    	var _posY =THREE.Math.randFloat(-10,10);
    	socket.position = new THREE.Vector3(_posX,_posY,0);
    	socket.lookAt = new THREE.Vector3(_posX,_posY-1,0);
    	socket.vUnit = new THREE.Vector3(0,-1,0);
    	socket.velocity = 0;
    };
    function setProperties(socket){
    	setInitialPosition(socket); 
    	socket.bullets = [];
    	socket.interval_newBullet_def = 10;
    	socket.interval_newBullet = socket.interval_newBullet_def;
    };
    function checkBulletOutside(bullet){
    	if(bullet.position.x>100 || bullet.position.x<-100){
    		bullet.visible = false;
    	}
    	if(bullet.position.y>100 || bullet.position.y<-100){
    		bullet.visible = false;
    	}
    };
    function checkSocketOutside(socket){
    	if(socket.position.x>70){
    		socket.position.x=70;
    	}
    	if(socket.position.x<-70){
    		socket.position.x=-70;
    	}
    	if(socket.position.y>70){
    		socket.position.y=70;
    	}
    	if(socket.position.y<-70){
    		socket.position.y=-70;
    	} 
    };

    var _fps = function(){
    	var _packs = [];
    	for(var _idx=0;_idx<_sockets.length;_idx++){
    		var _socket = _sockets[_idx];
    		if(_socket.connected){
    			if(_socket.interval_newBullet>0){
    				_socket.interval_newBullet--;
    			}else{
	    			if(_socket.prep_newBullet){
	    				createNewBullet(_socket);
	    				_socket.prep_newBullet = false;
	    			}
    				_socket.interval_newBullet = _socket.interval_newBullet_def;
    			}
	    		var _pack = {};
	    		_pack.userid = _socket.userid;
	    		_pack.lookAt = _socket.lookAt.toArray(); 
	    		_socket.vUnit = _socket.lookAt.clone().sub(_socket.position).normalize();
	        	var _distance = _socket.lookAt.distanceTo(_socket.position);
	        	_distance = THREE.Math.clamp(_distance,0,10);
	        	if(_distance>0.1){
	        		_socket.velocity = 0.1*(_distance/10);
	        	}else{
	        		_socket.velocity = 0;
	        	}
	        	_socket.position.addScaledVector(_socket.vUnit,_socket.velocity);
	        	checkSocketOutside(_socket);

	    		_pack.position = _socket.position.toArray();

	    		var _arr_bullets = [];

	    		for(var _idy=0;_idy<_socket.bullets.length;_idy++){
	    			var _bullet = _socket.bullets[_idy];
	    			if(_bullet.visible){
						_bullet.lookAt.copy(_bullet.position).addScaledVector(_bullet.direction,1); 
	    				_bullet.position.addScaledVector(_bullet.direction,_bullet.velocity);
	    				checkBulletOutside(_bullet);
	    			}
	    			if(_bullet.visible){ 
	    				_arr_bullets.push({
	    					position:_bullet.position.toArray(),
	    					direction:_bullet.direction.toArray(),
	    					lookAt:_bullet.lookAt.toArray() 
	    				});
	    			}
	    		}
	    		_pack.bullets = _arr_bullets;

	    		_packs.push(_pack);
    		}else{
    			_sockets.splice(_idx,1);
    		}
    	}
    	if(_packs.length>0){
    		io.emit("clients",_packs);
    		delete _packs;
    	} 
    };
    var _interval = setInterval(_fps,1000/60);